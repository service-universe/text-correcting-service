# Grammar service

Python service for analyzing stylistic and grammar mistakes in text.

# Routes

### /api/grammar

Route is waiting for POST requests with header:

#### Headers:

    application/json

#### Body:

    { "text": "foo" }

#### Response:

    {
    	[
    		{
    			"position": [
    				starting_position_of_word_in_text,
    				ending_position_of_word_in_text
    			],
    			"message": "mistake message",
    			"correct": [
    				"correct_form_of_mistaken_word_1",
    				"correct_form_of_mistaken_word_2"
    			]
    		}
    	]
    }

# Author

Radek Kucera | **radakuceru@gmail.com**

#Special thanks
**Python language_tool** - python library using java grammar tools for grammar analyzing
